<?php

/**
 * Field handler to provide simple renderer that allows linking to a node.
 */
class views_handler_field_formatted_virtual extends views_handler_field_formatted {

  function query() {
    // do nothing -- to override the parent query.
  }
    
}